
CREATE DATABASE `entidad`;

USE `entidad`;

DROP TABLE IF EXISTS `informacion`;

CREATE TABLE IF NOT EXISTS `informacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `nit` int(20) NOT NULL,
  `fecha` varchar(10) NOT NULL,
  `direccion` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;


INSERT INTO `informacion` (`id`, `nombre`, `nit`, `fecha`, `direccion`) VALUES
(1, 'AEON', 333221, '15-06-2001', 'San Salvador, El Salvador')