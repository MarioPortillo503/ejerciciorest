package com.crud.entidad;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudEntidadApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudEntidadApplication.class, args);
	}

}
