/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crud.entidad.nego;

import com.crud.entidad.models.Entidad;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Mario
 */
@Repository
public class EntidadDAO {
    
    @PersistenceContext
    EntityManager entM;
    
    @Transactional(readOnly = true)
    public List<Entidad> obtenerListado(){
        Query query = entM.createQuery("select entidad from Entidad entidad");
        return query.getResultList();
    }
    @Transactional(readOnly = true)
    public Entidad obtenerEntidadById(Integer id){
        Entidad entidad = entM.find(Entidad.class, id);
        return entidad;
    }
    @Transactional
    public void agregar(Entidad entidad){
        entM.persist(entidad);
    }
    
    @Transactional
    public void modificar(Entidad entidad){
        entM.merge(entidad);
    }
    
    @Transactional
    public void eliminar(Entidad entidad){
        entM.remove(entidad);
    }
    
 
}
