/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crud.entidad.controller;

import com.crud.entidad.models.Entidad;
import com.crud.entidad.nego.EntidadDAO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Mario
 */
@Controller
@RestController
@RequestMapping("/entidad")
public class EmpresaController {
    
    @Autowired
    public EntidadDAO entidadDao;
    
    
    @RequestMapping(value = "/obtener")
    @ResponseBody
    public List<Entidad> obtenerListado(){
        return entidadDao.obtenerListado();
    }
    
    @PostMapping(value = "/agregar")
    @ResponseBody
    public ResponseEntity agregar( @RequestBody Entidad entidad){
        try{
            entidadDao.agregar(entidad);
            return ResponseEntity.ok("Entidad Empresarial Creada Exitosamente");
        }catch(Exception e){
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
    
    @GetMapping(value = "/buscar")
    @ResponseBody
    public ResponseEntity listar(@RequestParam int id){
        try{
            Entidad empresa = entidadDao.obtenerEntidadById(id);
            if(empresa == null){
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Entidad Empresarial No Existe");
            }
             return ResponseEntity.ok(empresa);
        }catch(Exception e){
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
    
    @PutMapping(value = "/modificar")
    public ResponseEntity actualizar( @RequestBody Entidad entidad){
        try{
            entidadDao.modificar(entidad);
            return  ResponseEntity.ok("Entidad Empresarial Modificada Exitosamente");
        }catch(Exception e){
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
    
    @DeleteMapping(value = "/eliminar")
    public ResponseEntity actualizar(@RequestParam int id){
        try{
            Entidad entidad = entidadDao.obtenerEntidadById(id);
            if(entidad == null){
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Entidad Empresarial No Existe");
            }
            entidadDao.eliminar(entidad);
            return  ResponseEntity.ok("Entidad Empresarial Eliminada Exitosamente");
        }catch(Exception e){
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
    
   
}
